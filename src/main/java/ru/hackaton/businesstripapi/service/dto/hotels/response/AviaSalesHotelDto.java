package ru.hackaton.businesstripapi.service.dto.hotels.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AviaSalesHotelDto {
    private final String hotelName;
    private final AviaSalesHotelLocationDto hotelLocation;
    private final String stars;
    private String pricePeriod;
    private String priceDay;

    @JsonCreator
    public AviaSalesHotelDto(
            @JsonProperty("hotelName") String hotelName,
            @JsonProperty("location") AviaSalesHotelLocationDto hotelLocation,
            @JsonProperty("priceFrom") String pricePeriod,
            @JsonProperty("stars") String stars
    ) {
        this.hotelName = hotelName;
        this.hotelLocation = hotelLocation;
        this.stars = stars;
        this.pricePeriod = pricePeriod;
    }

    public String getHotelName() {
        return hotelName;
    }

    public AviaSalesHotelLocationDto getHotelLocation() {
        return hotelLocation;
    }

    public String getStars() {
        return stars;
    }

    public String getPricePeriod() {
        return pricePeriod;
    }

    public void setPricePeriod(String pricePeriod) {
        this.pricePeriod = pricePeriod;
    }

    public String getPriceDay() {
        return priceDay;
    }

    public void setPriceDay(String priceDay) {
        this.priceDay = priceDay;
    }

    @Override
    public String toString() {
        return "AviaSalesHotelDto{" +
                "hotelName='" + hotelName + '\'' +
                ", hotelLocation=" + hotelLocation +
                ", pricePeriod='" + pricePeriod + '\'' +
                ", priceDay='" + priceDay + '\'' +
                '}';
    }
}
