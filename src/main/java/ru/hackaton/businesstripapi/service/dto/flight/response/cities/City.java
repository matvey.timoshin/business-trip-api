package ru.hackaton.businesstripapi.service.dto.flight.response.cities;

import com.fasterxml.jackson.annotation.JsonProperty;

public class City {
    private String name;
    private String iata;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }
}
