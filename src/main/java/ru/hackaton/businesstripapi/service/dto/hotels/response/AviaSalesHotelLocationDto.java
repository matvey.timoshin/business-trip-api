package ru.hackaton.businesstripapi.service.dto.hotels.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AviaSalesHotelLocationDto {
    private final String locationName;
    private final String locationCountry;

    @JsonCreator
    public AviaSalesHotelLocationDto(@JsonProperty("name") String locationName, @JsonProperty("country") String locationCountry) {
        this.locationName = locationName;
        this.locationCountry = locationCountry;
    }

    public String getLocationName() {
        return locationName;
    }

    public String getLocationCountry() {
        return locationCountry;
    }

    @Override
    public String toString() {
        return "AviaSalesHotelLocationDto{" +
                "locationName='" + locationName + '\'' +
                ", locationCountry='" + locationCountry + '\'' +
                '}';
    }
}
