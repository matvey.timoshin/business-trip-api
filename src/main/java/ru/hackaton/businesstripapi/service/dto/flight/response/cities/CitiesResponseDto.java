package ru.hackaton.businesstripapi.service.dto.flight.response.cities;

import java.util.List;

public class CitiesResponseDto {
    List<City> cities;

    public CitiesResponseDto(List<City> cities) {
        this.cities = cities;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }
}
