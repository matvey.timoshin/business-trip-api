package ru.hackaton.businesstripapi.service.dto.flight.response.flights;

public class Flight {
    private String price;
    private Point outboundData;
    private Point inboundData;
    private String quoteDateTime;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Point getOutboundData() {
        return outboundData;
    }

    public void setOutboundData(Point outboundData) {
        this.outboundData = outboundData;
    }

    public Point getInboundData() {
        return inboundData;
    }

    public void setInboundData(Point inboundData) {
        this.inboundData = inboundData;
    }

    public String getQuoteDateTime() {
        return quoteDateTime;
    }

    public void setQuoteDateTime(String quoteDateTime) {
        this.quoteDateTime = quoteDateTime;
    }
}
