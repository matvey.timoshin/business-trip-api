package ru.hackaton.businesstripapi.service.dto.flight.response.flights;

import java.util.List;

public class FlightsResponseDto {
    private List<Flight> fligths;

    public FlightsResponseDto(List<Flight> fligths) {
        this.fligths = fligths;
    }

    public List<Flight> getFligths() {
        return fligths;
    }

    public void setFligths(List<Flight> fligths) {
        this.fligths = fligths;
    }
}
