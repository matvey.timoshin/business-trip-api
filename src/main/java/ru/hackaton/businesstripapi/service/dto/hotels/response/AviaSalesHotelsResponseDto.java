package ru.hackaton.businesstripapi.service.dto.hotels.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AviaSalesHotelsResponseDto {
    private List<AviaSalesHotelDto> aviaSalesHotelDtoList;

    @JsonCreator
    public AviaSalesHotelsResponseDto(List<AviaSalesHotelDto> aviaSalesHotelDtoList) {
        this.aviaSalesHotelDtoList = aviaSalesHotelDtoList;
    }

    @JsonProperty("hotels")
    public List<AviaSalesHotelDto> getAviaSalesHotelDtoList() {
        return aviaSalesHotelDtoList;
    }

    public void setAviaSalesHotelDtoList(List<AviaSalesHotelDto> aviaSalesHotelDtoList) {
        this.aviaSalesHotelDtoList = aviaSalesHotelDtoList;
    }

    @Override
    public String toString() {
        return "AviaSalesHotelsResponseDto{" +
                "aviaSalesHotelDtoList=" + aviaSalesHotelDtoList +
                '}';
    }
}
