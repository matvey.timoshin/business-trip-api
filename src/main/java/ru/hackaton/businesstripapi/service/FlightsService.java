package ru.hackaton.businesstripapi.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.hackaton.businesstripapi.service.dto.flight.response.flights.Flight;
import ru.hackaton.businesstripapi.service.dto.flight.response.flights.FlightsResponseDto;
import ru.hackaton.businesstripapi.service.dto.flight.response.cities.CitiesResponseDto;
import ru.hackaton.businesstripapi.service.dto.flight.response.cities.City;
import ru.hackaton.businesstripapi.service.dto.flight.response.flights.Point;

import java.net.URI;
import java.util.*;

@Service
public class FlightsService {
    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;

    public FlightsService(RestTemplate restTemplate, ObjectMapper objectMapper) {
        this.restTemplate = restTemplate;
        this.objectMapper = objectMapper;
    }

    public FlightsResponseDto getFlightsData(String originPlace, String destinationPlace, String outboundPartialDate, String inboundPartialDate) {
        List<Flight> flights = new ArrayList<>();
        try {
            Map<String, String> uriParams = new HashMap<>();
            uriParams.put("country", "RU");
            uriParams.put("currency", "rub");
            uriParams.put("locale", "en-EN");
            uriParams.put("originPlace", originPlace);
            uriParams.put("destinationPlace", destinationPlace);
            uriParams.put("outboundPartialDate", outboundPartialDate);
            uriParams.put("inboundPartialDate", inboundPartialDate);

            URI uri = UriComponentsBuilder.fromUriString("http://partners.api.skyscanner.net/apiservices/browsequotes/v1.0/{country}/{currency}/{locale}/{originPlace}/{destinationPlace}/{outboundPartialDate}/{inboundPartialDate}?apikey=prtl6749387986743898559646983194")
                    .buildAndExpand(uriParams).toUri();

            System.out.println(uri.toString());

            String str = restTemplate.exchange(
                    UriComponentsBuilder.fromUri(uri).toUriString(),
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<String>() {
                    },
                    uriParams
            ).getBody();

            JsonNode jsonNode = objectMapper.readTree(str);
            // Получаем ассоциативный массив городов
            HashMap<Integer, String> citiesMap = new HashMap<>();
            JsonNode places = jsonNode.get("Places");
            for (JsonNode place : places) {
                JsonNode placeId = place.findValue("PlaceId");
                JsonNode cityName = place.findValue("CityName");
                if (placeId == null || cityName == null) {
                    continue;
                }
                citiesMap.put(placeId.asInt(), cityName.asText());
            }

            // Получаем ассоциативный массив авиакомпаний
            HashMap<Integer, String> careersMap = new HashMap<>();
            JsonNode carriers = jsonNode.get("Carriers");
            for (JsonNode carrier : carriers) {
                JsonNode careerId = carrier.findValue("CarrierId");
                JsonNode careerName = carrier.findValue("Name");
                if (careerId == null || careerName == null) {
                    continue;
                }
                careersMap.put(careerId.asInt(), careerName.asText());
            }

            // Получаем коллекцию авиарейсов
            JsonNode quotes = jsonNode.get("Quotes");
            Flight flight = new Flight();

            for (JsonNode quote : quotes) {
                JsonNode price = quote.findValue("MinPrice");
                JsonNode direct = quote.findValue("Direct");
                JsonNode quoteDateTime = quote.findValue("QuoteDateTime");
                if (price == null || quoteDateTime == null || direct == null || !direct.asBoolean()) {
                    continue;
                }

                JsonNode outboundLeg = quote.findValue("OutboundLeg");
                JsonNode inboundLeg = quote.findValue("InboundLeg");
                if (outboundLeg == null || inboundLeg == null) {
                    continue;
                }

                Point outboundData = getPointFromJsonNode(citiesMap, careersMap, outboundLeg);
                Point inboundData = getPointFromJsonNode(citiesMap, careersMap, inboundLeg);

                if (outboundData == null || inboundData == null) {
                    continue;
                }

                flight.setPrice(price.asText());
                flight.setOutboundData(outboundData);
                flight.setInboundData(inboundData);
                flight.setQuoteDateTime(quoteDateTime.asText());
                flights.add(flight);
            }

        } catch (Exception e) {
            System.out.println(e);
        }

        return new FlightsResponseDto(flights);
    }

    private Point getPointFromJsonNode(HashMap<Integer, String> citiesMap, HashMap<Integer, String> careersMap, JsonNode boundData) {
        Point point = new Point();
        JsonNode originId = boundData.findValue("OriginId");
        JsonNode destinationId = boundData.findValue("DestinationId");
        JsonNode carrierId = boundData.findValue("CarrierIds").get(0);
        JsonNode departureDate = boundData.findValue("DepartureDate");
        if (originId == null || destinationId == null || departureDate == null) {
            return null;
        }
        point.setCarrierName(careersMap.get(carrierId.asInt()));
        point.setOriginPlaceName(citiesMap.get(originId.asInt()));
        point.setDestinationPlaceName(citiesMap.get(destinationId.asInt()));
        point.setDepartureDate(departureDate.asText());

        return point;
    }

    public CitiesResponseDto getCities() {
        List<City> result = new ArrayList<>();
        try {
            URI uri = UriComponentsBuilder.fromUriString("http://partners.api.skyscanner.net/apiservices/geo/v1.0?apikey=prtl6749387986743898559646983194")
                    .build().toUri();

            String str = restTemplate.exchange(
                    UriComponentsBuilder.fromUri(uri).toUriString(),
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<String>() {
                    }
            ).getBody();

            JsonNode jsonNode = objectMapper.readTree(str);

            JsonNode continents = jsonNode.get("Continents");
            for (JsonNode continent : continents) {
                JsonNode countries = continent.get("Countries");
                for (JsonNode country : countries) {
                    JsonNode cities = country.get("Cities");
                    for (JsonNode city : cities) {
                        City newCity = new City();
                        JsonNode iataString = city.findValue("IataCode");
                        JsonNode nameString = city.findValue("Name");
                        if (iataString == null || nameString == null) {
                            continue;
                        }
                        newCity.setIata(iataString.asText());
                        newCity.setName(nameString.asText());
                        result.add(newCity);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return new CitiesResponseDto(result);
    }
}
