package ru.hackaton.businesstripapi.service;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.stereotype.Service;
import ru.hackaton.businesstripapi.web.dto.hotels.request.FlightsDataDto;
import ru.hackaton.businesstripapi.web.dto.hotels.request.GetDocumentRequestDto;
import ru.hackaton.businesstripapi.web.dto.hotels.request.HotelsDataDto;

import java.io.ByteArrayOutputStream;

@Service
public class FileService {

    public byte[] getFileAsPdf(GetDocumentRequestDto getDocumentRequestDto) throws Exception {
        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            if (getDocumentRequestDto == null
                    || (getDocumentRequestDto.getHotelsData() == null && getDocumentRequestDto.getFlightsData() == null)) {
                return null;
            }
            Document document = new Document();
            PdfWriter.getInstance(document, byteArrayOutputStream);
            document.open();
            if (getDocumentRequestDto.getFlightsData() != null && getDocumentRequestDto.getFlightsData().getIn() != null) {
                Paragraph paragraph = new Paragraph("Flights");
                paragraph.setAlignment(Element.ALIGN_CENTER);
                document.add(paragraph);
                paragraph = new Paragraph("Flight in");
                paragraph.setAlignment(Element.ALIGN_CENTER);
                document.add(paragraph);
                FlightsDataDto flightsDataDto = getDocumentRequestDto.getFlightsData();
                Paragraph data = new Paragraph();
                Phrase phrase = new Phrase();
                if (flightsDataDto.getIn().getPointOfDeparture() != null)
                    phrase.add(new Chunk("Point of departure: " + flightsDataDto.getIn().getPointOfDeparture()));
                data.add(phrase);
                document.add(data);

                phrase = new Phrase();
                data = new Paragraph();
                if (flightsDataDto.getIn().getDestination() != null)
                    phrase.add(new Chunk("Destination: " + flightsDataDto.getIn().getDestination()));
                data.add(phrase);
                document.add(data);

                phrase = new Phrase();
                data = new Paragraph();
                if (flightsDataDto.getIn().getDepartureDate() != null)
                    phrase.add(new Chunk("Departure date: " + flightsDataDto.getIn().getDepartureDate()));
                data.add(phrase);
                document.add(data);

                /*phrase = new Phrase();
                data = new Paragraph();
                if (flightsDataDto.getIn().getReturnFlightDate() != null)
                    phrase.add(new Chunk("Return flight date: " + flightsDataDto.getIn().getReturnFlightDate()));
                data.add(phrase);
                document.add(data);
*/
                phrase = new Phrase();
                data = new Paragraph();
                if (flightsDataDto.getIn().getAirline() != null)
                    phrase.add(new Chunk("Airline: " + flightsDataDto.getIn().getAirline()));
                data.add(phrase);
                document.add(data);

                /*phrase = new Phrase();
                data = new Paragraph();
                if (flightsDataDto.getIn().getTicketPrice() != null)
                    phrase.add(new Chunk("Ticket price: " + flightsDataDto.getIn().getTicketPrice()));
                data.add(phrase);
                document.add(data);*/
            }
            if (getDocumentRequestDto.getHotelsData() != null && getDocumentRequestDto.getFlightsData().getOut() != null) {
                /*Paragraph paragraph = new Paragraph("Flight");
                paragraph.setAlignment(Element.ALIGN_CENTER);
                document.add(paragraph);*/
                Paragraph paragraph = new Paragraph("Flight out");
                paragraph.setAlignment(Element.ALIGN_CENTER);
                document.add(paragraph);
                FlightsDataDto flightsDataDto = getDocumentRequestDto.getFlightsData();
                Paragraph data = new Paragraph();
                Phrase phrase = new Phrase();
                if (flightsDataDto.getOut().getPointOfDeparture() != null)
                    phrase.add(new Chunk("Point of departure: " + flightsDataDto.getOut().getPointOfDeparture()));
                data.add(phrase);
                document.add(data);

                phrase = new Phrase();
                data = new Paragraph();
                if (flightsDataDto.getOut().getDestination() != null)
                    phrase.add(new Chunk("Destination: " + flightsDataDto.getOut().getDestination()));
                data.add(phrase);
                document.add(data);

                phrase = new Phrase();
                data = new Paragraph();
                if (flightsDataDto.getOut().getDepartureDate() != null)
                    phrase.add(new Chunk("Departure date: " + flightsDataDto.getOut().getDepartureDate()));
                data.add(phrase);
                document.add(data);

                /*phrase = new Phrase();
                data = new Paragraph();
                if (flightsDataDto.getOut().getReturnFlightDate() != null)
                    phrase.add(new Chunk("Return flight date: " + flightsDataDto.getOut().getReturnFlightDate()));
                data.add(phrase);
                document.add(data);*/

                phrase = new Phrase();
                data = new Paragraph();
                if (flightsDataDto.getOut().getAirline() != null)
                    phrase.add(new Chunk("Airline: " + flightsDataDto.getOut().getAirline()));
                data.add(phrase);
                document.add(data);
                phrase = new Phrase();
                data = new Paragraph();
                phrase.add(new Chunk(" "));
                data.add(phrase);
                document.add(data);
                if (getDocumentRequestDto.getFlightsData().getPrice()!= null)
                    phrase.add(new Chunk("Ticket price: " + getDocumentRequestDto.getFlightsData().getPrice()));
                data.add(phrase);
                document.add(data);
            }
            if (getDocumentRequestDto.getHotelsData() != null) {
                Paragraph data = new Paragraph();
                Phrase phrase = new Phrase();
                Paragraph paragraph = new Paragraph("Hotel");
                paragraph.setAlignment(Element.ALIGN_CENTER);
                document.add(paragraph);
                HotelsDataDto hotelsDataDto = getDocumentRequestDto.getHotelsData();
                phrase = new Phrase();
                data = new Paragraph();
                if (hotelsDataDto.getHotelName() != null)
                    phrase.add(new Chunk("Hotel name: " + hotelsDataDto.getHotelName()));
                data.add(phrase);
                document.add(data);

                phrase = new Phrase();
                data = new Paragraph();
                if (hotelsDataDto.getHotelCostPeriod() != null)
                    phrase.add(new Chunk("Hotel cost for the period: " + hotelsDataDto.getHotelCostPeriod()));
                data.add(phrase);
                document.add(data);

                phrase = new Phrase();
                data = new Paragraph();
                if (hotelsDataDto.getHotelCostDay() != null)
                    phrase.add(new Chunk("Hotel cost per day: " + hotelsDataDto.getHotelCostDay()));
                data.add(phrase);
                document.add(data);

                phrase = new Phrase();
                data = new Paragraph();
                if (hotelsDataDto.getArrivalTime() != null)
                    phrase.add(new Chunk("Arrival time: " + hotelsDataDto.getArrivalTime()));
                data.add(phrase);
                document.add(data);

                phrase = new Phrase();
                data = new Paragraph();
                if (hotelsDataDto.getDepartureTime() != null)
                    phrase.add(new Chunk("Departure time: " + hotelsDataDto.getDepartureTime()));
                data.add(phrase);
                document.add(data);
            }
            document.close();
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e) {
            throw new Exception();
        }
    }
}
