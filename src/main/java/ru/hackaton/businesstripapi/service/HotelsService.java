package ru.hackaton.businesstripapi.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.hackaton.businesstripapi.service.dto.hotels.response.AviaSalesHotelDto;
import ru.hackaton.businesstripapi.service.dto.hotels.response.AviaSalesHotelsResponseDto;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class HotelsService {
    private final int MAX_PRICE_DAY = 7000;
    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;

    public HotelsService(RestTemplate restTemplate, ObjectMapper objectMapper) {
        this.restTemplate = restTemplate;
        this.objectMapper = objectMapper;
    }

    public AviaSalesHotelsResponseDto getDisplayAllowHotels(String location, String checkIn, String checkOut) throws Exception {
        try {
            String url = UriComponentsBuilder.fromUriString("http://engine.hotellook.com/api/v2/cache.json?currency=rub&token=5f77d528ab327820711bc54947eb3656")
                    .queryParam("location", location)
                    .queryParam("checkIn", checkIn)
                    .queryParam("checkOut", checkOut)
                    .toUriString();
            String response = restTemplate.exchange(
                    UriComponentsBuilder.fromUriString(url).toUriString(),
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<String>() {
                    }
            ).getBody();
            AviaSalesHotelsResponseDto aviaSalesHotelsResponseDto = objectMapper.readValue(response, AviaSalesHotelsResponseDto.class);
            int countDays = Integer.parseInt(checkOut.substring(8)) - Integer.parseInt(checkIn.substring(8));
            List<AviaSalesHotelDto> hotels = aviaSalesHotelsResponseDto.getAviaSalesHotelDtoList();
            aviaSalesHotelsResponseDto.setAviaSalesHotelDtoList(calcPriceDay(hotels, countDays));
            aviaSalesHotelsResponseDto.setAviaSalesHotelDtoList(sortHotelsByPrice(hotels));
            return aviaSalesHotelsResponseDto;
        } catch (Exception e) {
            throw new Exception();
        }
    }

    private List<AviaSalesHotelDto> calcPriceDay(List<AviaSalesHotelDto> hotels, int countDays) {
        for (AviaSalesHotelDto hotel : hotels) {
            double priceDay = Double.parseDouble(hotel.getPricePeriod()) / countDays;
            hotel.setPriceDay(Double.toString(priceDay));
        }
        return hotels;
    }

    private List<AviaSalesHotelDto> sortHotelsByPrice(List<AviaSalesHotelDto> hotels) {
        return hotels.stream()
                .filter(aviaSalesHotelDto -> Double.parseDouble(aviaSalesHotelDto.getPriceDay()) <= MAX_PRICE_DAY)
                .collect(Collectors.toList());
    }
}
