package ru.hackaton.businesstripapi.web.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.hackaton.businesstripapi.service.FileService;
import ru.hackaton.businesstripapi.service.FlightsService;
import ru.hackaton.businesstripapi.service.HotelsService;
import ru.hackaton.businesstripapi.service.dto.flight.response.flights.FlightsResponseDto;
import ru.hackaton.businesstripapi.service.dto.hotels.response.AviaSalesHotelsResponseDto;
import ru.hackaton.businesstripapi.service.dto.flight.response.cities.CitiesResponseDto;
import ru.hackaton.businesstripapi.service.dto.hotels.response.AviaSalesHotelDto;
import ru.hackaton.businesstripapi.service.dto.hotels.response.AviaSalesHotelsResponseDto;
import ru.hackaton.businesstripapi.web.dto.GenericResponse;
import ru.hackaton.businesstripapi.web.dto.hotels.request.GetDocumentRequestDto;

import java.util.List;

@Api
@RestController
public class BusinessTripController {
    private final FlightsService flightsService;
    private final HotelsService hotelsService;
    private final FileService fileService;

    @Autowired
    public BusinessTripController(FlightsService flightsService, HotelsService hotelsService, FileService fileService) {
        this.flightsService = flightsService;
        this.hotelsService = hotelsService;
        this.fileService = fileService;
    }

    @GetMapping(value = "/flights")
    @ApiOperation("Получить список доступных авиа билетов")
    public ResponseEntity<GenericResponse<FlightsResponseDto>> getFunctionResult(
            @RequestParam("originPlace") String originPlace,
            @RequestParam("destinationPlace") String destinationPlace,
            @RequestParam("outboundPartialDate") String outboundPartialDate,
            @RequestParam("inboundPartialDate") String inboundPartialDate
    ) {
        return ResponseEntity.ok(new GenericResponse<>(flightsService.getFlightsData(originPlace, destinationPlace, outboundPartialDate, inboundPartialDate)));
    }

    @GetMapping(value = "/cities")
    @ApiOperation("Получить список городов")
    public ResponseEntity<GenericResponse<CitiesResponseDto>> getCities() {
        return ResponseEntity.ok(new GenericResponse<>(flightsService.getCities()));
    }

    @GetMapping("/hotels")
    @ApiOperation("Получить список доступных отелей")
    public ResponseEntity<GenericResponse<AviaSalesHotelsResponseDto>> getHotels(
            @RequestParam("location") String location,
            @RequestParam("checkIn") String checkIn,
            @RequestParam("checkOut") String checkOut
    ) throws Exception {
        return ResponseEntity.ok(new GenericResponse<>(hotelsService.getDisplayAllowHotels(location, checkIn, checkOut)));
    }

    @PostMapping("/download")
    @ApiOperation("Скачать файл")
    public Object getBusinessTripAsFile(@RequestBody GetDocumentRequestDto getDocumentRequestDto) throws Exception {
        byte[] data = fileService.getFileAsPdf(getDocumentRequestDto);
        if (data != null) {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE);
            headers.set(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment;filename=%s.pdf", "my-business-trip"));
            headers.setContentLength(data.length);
            return new HttpEntity<>(data, headers);
        } else {
            return ResponseEntity.ok(GenericResponse.error(1, "Error get pdf"));
        }
    }
}
