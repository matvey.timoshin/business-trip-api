package ru.hackaton.businesstripapi.web.dto.hotels.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HotelsDataDto {
    private final String hotelName;
    private final String hotelCostPeriod;
    private final String hotelCostDay;
    private final String arrivalTime;
    private final String departureTime;

    @JsonCreator
    public HotelsDataDto(
            @JsonProperty("hotelName") String hotelName,
            @JsonProperty("hotelCostPeriod") String hotelCostPeriod,
            @JsonProperty("hotelCostDay") String hotelCostDay,
            @JsonProperty("arrivalTime") String arrivalTime,
            @JsonProperty("departureTime") String departureTime
    ) {
        this.hotelName = hotelName;
        this.hotelCostPeriod = hotelCostPeriod;
        this.hotelCostDay = hotelCostDay;
        this.arrivalTime = arrivalTime;
        this.departureTime = departureTime;
    }

    public String getHotelName() {
        return hotelName;
    }

    public String getHotelCostDay() {
        return hotelCostDay;
    }

    public String getHotelCostPeriod() {
        return hotelCostPeriod;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    @Override
    public String toString() {
        return "HotelsDataDto{" +
                "hotelName='" + hotelName + '\'' +
                ", hotelCostPeriod='" + hotelCostPeriod + '\'' +
                ", hotelCostDay='" + hotelCostDay + '\'' +
                ", arrivalTime='" + arrivalTime + '\'' +
                ", departureTime='" + departureTime + '\'' +
                '}';
    }
}
