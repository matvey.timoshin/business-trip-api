package ru.hackaton.businesstripapi.web.dto.hotels.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetDocumentRequestDto {
    private final FlightsDataDto flightsData;
    private final HotelsDataDto hotelsData;

    @JsonCreator
    public GetDocumentRequestDto(
            @JsonProperty("flightsData") FlightsDataDto flightsData,
            @JsonProperty("hotelsData") HotelsDataDto hotelsData
    ) {
        this.flightsData = flightsData;
        this.hotelsData = hotelsData;
    }

    public FlightsDataDto getFlightsData() {
        return flightsData;
    }

    public HotelsDataDto getHotelsData() {
        return hotelsData;
    }

    @Override
    public String toString() {
        return "GetDocumentRequestDto{" +
                "flightsData=" + flightsData +
                ", hotelsData=" + hotelsData +
                '}';
    }
}
