package ru.hackaton.businesstripapi.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GenericResponse<T> {
    private final Integer errorCode;
    private final String errorMessage;
    private final T resultData;

    private GenericResponse(Integer errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.resultData = null;
    }

    public GenericResponse(T resultData) {
        this.errorCode = 0;
        this.errorMessage = null;
        this.resultData = resultData;
    }

    public static <T> GenericResponse<T> error(Integer errorCode, String errorMessage) {
        return new GenericResponse<>(errorCode, errorMessage);
    }

    @JsonProperty("errorCode")
    public Integer getErrorCode() {
        return errorCode;
    }

    @JsonProperty("errorMessage")
    public String getErrorMessage() {
        return errorMessage;
    }

    @JsonProperty("resultData")
    public T getResultData() {
        return resultData;
    }
}
