package ru.hackaton.businesstripapi.web.dto.hotels.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FlightsDataDto {
    private final FlightDto in;
    private final FlightDto out;
    private final String price;

    @JsonCreator
    public FlightsDataDto(
            @JsonProperty("in") FlightDto in,
            @JsonProperty("out") FlightDto out,
            @JsonProperty("price") String price
    ) {
        this.in = in;
        this.out = out;
        this.price = price;
    }

    public FlightDto getIn() {
        return in;
    }

    public FlightDto getOut() {
        return out;
    }

    public String getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "FlightsDataDto{" +
                "in=" + in +
                ", out=" + out +
                ", price='" + price + '\'' +
                '}';
    }
}
