package ru.hackaton.businesstripapi.web.dto.hotels.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FlightDto {
    private final String pointOfDeparture;
    private final String destination;
    private final String departureDate;
    private final String returnFlightDate;
    private final String airline;
    //private final String ticketPrice;

    @JsonCreator
    public FlightDto(
            @JsonProperty("pointOfDeparture") String pointOfDeparture,
            @JsonProperty("destination") String destination,
            @JsonProperty("departureDate") String departureDate,
            @JsonProperty("returnFlightDate") String returnFlightDate,
            @JsonProperty("airline") String airline/*,
            @JsonProperty("ticketPrice") String ticketPrice*/
    ) {
        this.pointOfDeparture = pointOfDeparture;
        this.destination = destination;
        this.departureDate = departureDate;
        this.returnFlightDate = returnFlightDate;
        this.airline = airline;
        //this.ticketPrice = ticketPrice;
    }

    public String getPointOfDeparture() {
        return pointOfDeparture;
    }

    public String getDestination() {
        return destination;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public String getReturnFlightDate() {
        return returnFlightDate;
    }

    public String getAirline() {
        return airline;
    }

    /*public String getTicketPrice() {
        return ticketPrice;
    }*/

    @Override
    public String toString() {
        return "FlightDto{" +
                "pointOfDeparture='" + pointOfDeparture + '\'' +
                ", destination='" + destination + '\'' +
                ", departureDate='" + departureDate + '\'' +
                ", returnFlightDate='" + returnFlightDate + '\'' +
                ", airline='" + airline + '\'' +
                //", ticketPrice='" + ticketPrice + '\'' +
                '}';
    }
}
