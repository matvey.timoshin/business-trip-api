package ru.hackaton.businesstripapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BusinessTripApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BusinessTripApiApplication.class, args);
	}

}
